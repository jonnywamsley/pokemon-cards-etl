

import scrapy


class TestPokemonListSpider(scrapy.Spider):
    # this spider monitors changes on the site scraped
    name = 'test_pokemon_list'

    def start_requests(self):
        # This starts the web scraper at the following website
        url = "https://bulbapedia.bulbagarden.net/wiki/List_of_Pok%C3%A9mon_by_National_Pok%C3%A9dex_number"
        yield scrapy.Request(url=url, callback=self.parse_pokemon_list)

    def test_output(self):
        # makes sure the selected td index contains the pokemon list, and it was not moved
        expected_pokemon = ['Bulbasaur', 'Ivysaur', 'Venusaur', 'Charmander', 'Charmeleon']
        return expected_pokemon == self.pokemon_list[:5]

    def test_len(self):
        # confirms first 8 gens are present when scraped
        TOTAL_GEN_EIGHT_POKEMON = 890
        return len(self.pokemon_list) >= TOTAL_GEN_EIGHT_POKEMON

    def tests(self):
        # checks if all tests performed are working
        if self.test_output() and self.test_len():
            return "passed"
        else:
            return "failed"

    def parse_pokemon_list(self, response):
        # this scrapes the page and performs the tests
        pokemon_list_raw = response.xpath("//tr[@style ='background:#FFF']/td/a/text()").getall()
        self.pokemon_list = list(dict.fromkeys(pokemon_list_raw))

        yield {
            'job': 'test_pokemon_list',
            'status': response,
            'tests_success': self.tests(),
            'test_output': self.test_output(),
            'test_len': self.test_len(),
        }
