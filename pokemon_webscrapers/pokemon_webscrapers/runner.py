from scrapy.cmdline import execute
import sys


def scrape(job_name):
    """
        runs a scrapy job executing a spider to yield the collected data
        The file name format is the job name with a json extension

    :param job_name:
        executes the spider with the job name

    :yields json data
    """

    execute(
        [
            'scrapy',
            'crawl',
            job_name,
            '-o',
            f'{job_name}.json',  # _{date.today()}
        ])


if __name__ == '__main__':
    scrape(sys.argv[1])
